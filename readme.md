Web Developer Task - 2015


1) Using PHP, generate and display a randomised 500 digit long number.
Now determine the greatest product of 4 consecutive digits, within this number.

2) Using Javascript / jQuery: 
Display the numbers from 1 - 1000 in a random order on the screen.
If the user hovers their mouse over a given number, highlight that numbers' 
divisors (ie those that divide evenly into it) 
Use CSS to ensure that the numbers are not just displayed in a static line, 
make it so that all 1000 numbers appear without the need to scroll. 

Eg if the user hovers their cursor over 21, the numbers 1, 3, 7 would be 
highlighted. 22 would highlight 1, 2 and 11. 
