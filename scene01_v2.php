<?php

echo "<h2>Sequential four consecutive number product</h2>";

$maxproduct = 0;
$maxloop = 500;

for ($i=1; $i <= $maxloop; $i++) { 
	echo "[$i]: ";
	$x[$i] = rand(0,9);
	echo $x[$i].'<br/>';
	if($i % 4 == 0) {
		$temp = 1;
		for ($j=$i-4; $j<=$i-1; $j++) {
			echo ($j<=$i) ? "<em>".$x[$j+1]." * </em>" : "<em>".$x[$j+1]."</em>";
			$temp *= $x[$j+1];
		}
		echo " = " . $temp;
		if($temp > $maxproduct) {
			$maxproduct = $temp;
			echo " --> Highest in this iteration: " . $maxproduct;
		}
		echo "<br/>";
	}
}

echo "<p><h3>Highest four consecutive product in this run: $maxproduct</h3></p>";