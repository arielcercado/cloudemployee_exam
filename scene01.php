<?php

echo "<h2>Overlapping four consecutive number product</h2>";

$maxproduct = 0;
$maxloop = 500;

for ($i=1; $i <= $maxloop; $i++) { 
	echo "[$i]: ";
	$x[$i] = rand(0,9);
	echo $x[$i];
	
	// we start our consecutive series on the fourth number
	if($i>3) {
		$temp = 1;
		echo " : ";
		for($j=1; $j<=4; $j++) {
			// echo "<em>".$x[($i-4+$j)]." * </em>";
			echo ($j<=3) ? "<em>".$x[($i-4+$j)]." * </em>" : "<em>".$x[($i-4+$j)]."</em>";
			$temp *= $x[($i-4+$j)];
		}
		echo " = " . $temp;
		if($temp > $maxproduct) {
			$maxproduct = $temp;
			echo " --> Highest in this iteration: " . $maxproduct;
		}
	}
	echo "<br/>";
}

echo "<p><h3>Highest four consecutive product in this run: $maxproduct</h3></p>";