<html>
<head>
	<meta charset="UTF-8">
	<title>Javascript test</title>

	<style>
		ul {
			list-style:none;
		}
		li {
			display: inline-block;
			margin-right: 5px;
			cursor: pointer;
		}
		.screenwidth {
			width: 100%;
			height: 100%;
			border: 1px solid black;
			overflow: scroll;
		}
		.emp {
			color: white;
			font-style: italic;
			background: black;
		}
		.selected {
			color: yellow;
		}
	</style>
</head>
<body>
	<div id="arrNum" class="screenwidth">

	</div>
	
	<script type="text/javascript" src="jquery-1.11.3.min.js"></script>
	<script type="text/javascript">
		// alert(21 % 7);
		// credits to Fisher-Yates (aka Knuth) Shuffle
		function shuffle(array) {
		  var currentIndex = array.length, temporaryValue, randomIndex ;

		  // While there remain elements to shuffle...
		  while (0 !== currentIndex) {

		    // Pick a remaining element...
		    randomIndex = Math.floor(Math.random() * currentIndex);
		    currentIndex -= 1;

		    // And swap it with the current element.
		    temporaryValue = array[currentIndex];
		    array[currentIndex] = array[randomIndex];
		    array[randomIndex] = temporaryValue;
		  }

		  return array;
		}

		function modulo(x) {
			for (i = 1; i <= x; i++) {
				if( x % i == 0) {
					// divisible, add css highlighting
					// $(#numArr).find(i);
				}
			}
		}

		var numArr = new Array (1000);
		for (i = 0; i <= 999; i++)
		{
			numArr[i] = i+1;
		}
		shuffle(numArr);
		console.log(numArr);
		
		dumpString = "";
		numArr.forEach(function(numX){
			console.log(numX);
			dumpString += "<li id=\"x_"+numX+"\">" + numX + "</li>";
		});

		$("#arrNum").prepend("<ul>"+dumpString+"</ul>");

		// apply highlighting on divisible numbers on selected item's value
		$('li').on('mouseover',function() {
			arrVal = $(this).text();
			$(this).addClass('selected');
			// console.log(arrVal);
			for (i = 1; i <= arrVal; i++) {
				// $("#x_1").addClass("emp");
				if( arrVal % i == 0) {
					console.log("li#"+i);
					$("#x_" + i).addClass('emp');
				}
			}
		});

		// reset everything to normal on mouseout
		$('li').on('mouseout',function() {
			$('li').removeClass('emp');
			$('li').removeClass('selected');
		});
	</script>
</body>
</html>